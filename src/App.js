import React, { useState } from 'react';
import scopusJson from './assets/scopus.json';

const App = () => {
  let [scopus, setScopus] = useState(JSON.stringify(scopusJson, null, 2));
  let [elastic, setElastic] = useState("");

  const baseUrl = "http://localhost:9200/import/"; // of the form "<server>/<index>/"

  const transformForElastic = () => {
    const jsonArr = JSON.parse(scopus);
    //console.log(jsonArr);
    let elas = {};
    jsonArr.forEach(elem => {
      if (!elas[elem.cristinid]) {
        console.log("Creating authid " + elem.authid + " under new Cristin id " + elem.cristinid);
        elas[elem.cristinid] = {
          authid: [elem.authid],
          name: [elem.name]
        };
      } else {
        console.log("Appending authid " + elem.authid + " to existing Cristin id " + elem.cristinid);
        elas[elem.cristinid].authid.push(elem.authid);
        elas[elem.cristinid].name.push(elem.name);
      }
    })
    //console.log(elastic)
    setElastic(JSON.stringify(elas, null, 2));
  }

  const clearElastic = () => {
    fetch(baseUrl, {
        method: 'DELETE'
    }).then(response => response.json()).then(json => {console.log(json)})
  }

  const populateElastic = () => {
    const json = JSON.parse(elastic)
    Object.keys(json).forEach( key => {
      const doc = json[key];
      console.log("processing key " + key, doc)
      fetch(baseUrl + "_doc/" + key, {
        method: 'PUT',
        headers: { "Content-Type": "application/json" }, //(new Headers()).append("Content-Type", "application/json"),
        body: JSON.stringify(doc)
    }).then(response => response.json()).then(json => {console.log(json)})
    })
  }

  return (
    <div>
      <h1>Cristin Import</h1>
      
      <h2>Synthetic input data annotated with CristinId (base data/training set)</h2>
      <textarea
        style={{minWidth: 400, minHeight: 300}}
        value={scopus}
        onChange={e => setScopus(e.target.value)}></textarea>
      <div>
        <button onClick={transformForElastic}>Transform</button>
      </div>
      
      <h2>Transformed into documents for Elastic</h2>
      <textarea
        style={{minWidth: 400, minHeight: 300}}
        value={elastic} disabled={true}></textarea>
      
      <h2>Put them in Elastic</h2>
      <div>
        <button onClick={clearElastic}>Clear</button>
        <button onClick={populateElastic}>Populate</button>
      </div>
    </div>
  );
}

export default App;